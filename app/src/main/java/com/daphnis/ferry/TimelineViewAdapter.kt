package com.daphnis.ferry

import android.content.Context
import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.daphnis.ferry.R
import com.github.vipulasri.timelineview.TimelineView
import kotlinx.android.synthetic.main.item_timeline.view.*


/**
 * Created by Your name on 10-03-2019.
 */
class TimelineViewAdapter(val context: Context) : RecyclerView.Adapter<TimeLineViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): TimeLineViewHolder {
        val view = parent.inflate(R.layout.item_timeline)
        return view
    }

    override fun getItemCount(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBindViewHolder(p0: TimeLineViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}



fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}


class TimeLineViewHolder(itemView: View, viewType: Int) : RecyclerView.ViewHolder(itemView) {
    val mTimeLineView: TimelineView

    init {
        mTimeLineView = itemView.timeline_item
        mTimeLineView.initLine(viewType)
    }

}
