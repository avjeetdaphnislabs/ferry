package com.daphnis.ferry.Retorfit

import com.google.gson.Gson
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Your name on 07-03-2019.
 */
class GeoCodeAPICient {

    companion object {
        const val BASE_URL = "https://maps.googleapis.com/maps/api/"

        private var retrofit : Retrofit? = null

        fun getAPICaller() : GeoCodeAPI{
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .build()

            return retrofit!!.create(GeoCodeAPI::class.java)
        }
    }


}