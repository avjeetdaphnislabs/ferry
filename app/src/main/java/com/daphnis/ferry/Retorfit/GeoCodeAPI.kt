package com.daphnis.ferry.Retorfit

import com.daphnis.ferry.Model.Result
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Avjeet on 07-03-2019.
 */
interface GeoCodeAPI {

    @GET("geocode/json")
    fun loadChanges(@Query("latlng") latLng: String, @Query("key") key: String): Call<Result>
}