package com.daphnis.ferry.Activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.daphnis.ferry.R
import kotlinx.android.synthetic.main.activity_otp.*

class OtpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "OTP"

        otp_num_text.text = " We will auto verify the OTP sent to +91 ${intent.getStringExtra("phone")}"

        et1.requestFocus()

        et1.addTextChangedListener(GenericTextWatcher(et1))
        et2.addTextChangedListener(GenericTextWatcher(et2))
        et3.addTextChangedListener(GenericTextWatcher(et3))
        et4.addTextChangedListener(GenericTextWatcher(et4))

    }

    inner class GenericTextWatcher(private var view: View) : TextWatcher {

        override fun afterTextChanged(editable: Editable?) {
            val text = editable.toString()
            when (view.id) {
                R.id.et1 -> if (text.length == 1)
                    et2.requestFocus()
                R.id.et2 -> if (text.length == 1)
                    et3.requestFocus()
                R.id.et3 -> if (text.length == 1)
                    et4.requestFocus()
                R.id.et4 -> {
                    if (text.length == 1) {
                        progress_circle.visibility = View.VISIBLE
                        startActivity(Intent(this@OtpActivity, PermissionActivity::class.java))
                    }
                }

            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            //
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            //
        }
    }
}
