package com.daphnis.ferry.Activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.daphnis.ferry.R
import kotlinx.android.synthetic.main.activity_on_boarding.*

class OnBoardingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)

        btn_customer_login.setOnClickListener{
            startActivity(Intent(this@OnBoardingActivity, WelcomeScreenActivity::class.java))
        }

    }
}
