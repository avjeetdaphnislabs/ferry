package com.daphnis.ferry.Activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.daphnis.ferry.R
import kotlinx.android.synthetic.main.activity_welcome_screen.*

class WelcomeScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_screen)

        btn_next.setOnClickListener {
            val intent  =  Intent(this@WelcomeScreenActivity, OtpActivity::class.java)
            val phnNumber : String  = phn_num.text.toString()

            if(checkPhone(phnNumber)){
                intent.putExtra("phone",phnNumber)
                startActivity(intent)
            }
        }

    }

    private fun checkPhone(phn : String): Boolean{
        if(phn.length!=10){
            phn_num.error = "Number Not valid"
            return false
        }else
            return true
    }
}