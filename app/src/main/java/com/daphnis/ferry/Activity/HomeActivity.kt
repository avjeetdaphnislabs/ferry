package com.daphnis.ferry.Activity

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.NavigationView
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import com.daphnis.ferry.MapBottomFragment.ModeFragment
import com.daphnis.ferry.MapBottomFragment.SeatsFragment
import com.daphnis.ferry.Model.Result
import com.daphnis.ferry.R
import com.daphnis.ferry.Retorfit.GeoCodeAPICient
import com.daphnis.ferry.Util.GPSLogger
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.bottomsheet.*
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.location_card.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {
    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private var isDropActive: Boolean = false


    private val locationUpdateListener by lazy {
        object : LocationUpdate {
            override fun onLocationUpdate(location: Location) {
                val latLng = LatLng(location.latitude, location.longitude)
                mMap.addMarker(MarkerOptions().position(latLng))
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f))


            }
        }

    }

    private val gpsLogger by lazy {
        GPSLogger(this, (this.getSystemService(Context.LOCATION_SERVICE) as LocationManager), locationUpdateListener)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }

    public lateinit var bottomSheet: BottomSheetBehavior<LinearLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

//        toolbar.title = "Dashboard"
//        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.app_blue))
//        setSupportActionBar(toolbar)


        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        bottomSheet = BottomSheetBehavior.from(bottom_sheet)
        bottomSheet.isHideable = true

        bottomSheet.state = BottomSheetBehavior.STATE_EXPANDED
        val fragment = ModeFragment()
        supportFragmentManager.beginTransaction().add(R.id.fragment_container, fragment).commit()


    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


    @SuppressLint("MissingPermission")
    fun viewEditLocation() {

        bottomSheet.state = BottomSheetBehavior.STATE_HIDDEN

        //gpsLogger.startCaptureLocation()

        LayoutInflater.from(this).inflate(R.layout.location_card, content_home_rl)
        activatePickupText()

        pick_rl.setOnClickListener {
            activatePickupText()
        }


        drop_rl.setOnClickListener {
            activateDropText()
        }

        setUpMap()

    }


    @SuppressLint("MissingPermission")
    private fun setUpMap() {
        mMap.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location
                Log.i("c", "c")
                val currentLatLng = LatLng(location.latitude, location.longitude)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
                  getAddress(currentLatLng)
            }
        }
    }

    override fun onStop() {
        super.onStop()

        gpsLogger.removeUpdate()
    }

    interface LocationUpdate {
        fun onLocationUpdate(location: Location)
    }

    private fun getAddress(latLng: LatLng): String? {
        var address: String? = null
        val revGeoCodeCaller = GeoCodeAPICient.getAPICaller()
        val caller = revGeoCodeCaller.loadChanges(
            "${latLng.latitude},${latLng.longitude}",
            resources.getString(R.string.google_geocode_key)
        )


        return address
    }

    fun activatePickupText() {
        isDropActive = false
        drop_text_title.visibility = View.GONE
        drop_card.cardElevation = 2f
        pickup_card.cardElevation = 4f
        pick_text_title.visibility = View.VISIBLE
        pickup_card.bringToFront()
        drop_rl.setBackgroundColor(ContextCompat.getColor(this@HomeActivity, R.color.light_grey))
        pick_rl.setBackgroundColor(ContextCompat.getColor(this@HomeActivity, R.color.white))
    }

    fun activateDropText() {
        if (!isDropActive) {
            isDropActive = true

            pick_text_title.visibility = View.GONE
            drop_text_title.visibility = View.VISIBLE
            drop_card.cardElevation = 4f
            pickup_card.cardElevation = 2f
            drop_card.bringToFront()
            pick_rl.setBackgroundColor(ContextCompat.getColor(this@HomeActivity, R.color.light_grey))
            drop_rl.setBackgroundColor(ContextCompat.getColor(this@HomeActivity, R.color.white))
        } else {
            bottomSheet.state = BottomSheetBehavior.STATE_EXPANDED
            val fragment = SeatsFragment()
            supportFragmentManager.beginTransaction().add(R.id.fragment_container, fragment).commit()
        }

    }
}
