package com.daphnis.ferry.MapBottomFragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.daphnis.ferry.R
import kotlinx.android.synthetic.main.fragment_seats.*
import kotlinx.android.synthetic.main.fragment_seats.view.*


class SeatsFragment : Fragment() {
    companion object {
        private var numSeats = 0
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_seats, container, false)

        view.inc_op.setOnClickListener {
            numSeats =  Integer.parseInt(view.seats_tv.text.toString())
            view.seats_tv.text = incDecSeats(numSeats,true).toString()
        }
        view.dec_op.setOnClickListener {
            numSeats =  Integer.parseInt(view.seats_tv.text.toString())
            view.seats_tv.text = incDecSeats(numSeats,false).toString()
        }

        btn_confirm.setOnClickListener {

        }

        return view
    }

    private fun incDecSeats(num : Int, isIncrease: Boolean): Int{
        if(isIncrease){
            if(num !=4)
            return num+1
        }else {
            if (num != 1)
                return num - 1
        }
        return num
    }
}
