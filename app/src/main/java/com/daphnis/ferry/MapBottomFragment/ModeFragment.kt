package com.daphnis.ferry.MapBottomFragment

import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.daphnis.ferry.Activity.HomeActivity
import com.daphnis.ferry.R
import kotlinx.android.synthetic.main.fragment_mode.view.*


class ModeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_mode, container, false)

        view.btn_confirm.setOnClickListener {
            (activity as HomeActivity).bottomSheet.state = BottomSheetBehavior.STATE_COLLAPSED
            (activity as HomeActivity).viewEditLocation()
        }

        return view
    }


}
