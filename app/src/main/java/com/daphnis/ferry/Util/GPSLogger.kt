package com.daphnis.ferry.Util

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast
import com.daphnis.ferry.Activity.HomeActivity


/**
 * Created by Avjeet on 22-02-2019.
 */
class GPSLogger(val context: Context, val locationManager: LocationManager, val locListener: HomeActivity.LocationUpdate) {
    private var bestLocation : Location? = null
    private var locationReady: Boolean = false
    private val locationListener = object : LocationListener {
        override fun onLocationChanged(location: Location?) {
//            locationReady = true
//            bestLocation = location
            location?.let {
                locListener.onLocationUpdate(location)
            }
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle?) {
           // Toast.makeText(context, "Provide changed",Toast.LENGTH_LONG).show()
        }

        override fun onProviderEnabled(provider: String) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onProviderDisabled(provider: String) {
            context.startActivity(Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        }
    }


    companion object {
        private const val UPDATE_INTERVAL_IN_MILLISECONDS :Long = 400
        private const val MIN_DISTANCE_CHANGE_FOR_UPDATES :Float= 1f
    }



    @SuppressLint("MissingPermission")
    fun startCaptureLocation(){
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
            UPDATE_INTERVAL_IN_MILLISECONDS,
            MIN_DISTANCE_CHANGE_FOR_UPDATES,locationListener)
    }

    fun removeUpdate() {
        locationReady = false
        locationManager.removeUpdates(locationListener)
    }

    fun isGpsEnabled() : Boolean{
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    @SuppressLint("MissingPermission")
    fun getDeviceLocation() : Location{
        if (bestLocation == null) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    UPDATE_INTERVAL_IN_MILLISECONDS,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES,locationListener)
                return if (locationReady) {
                    locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                } else {
                    dummyLocation()
                }
        } else {
            return bestLocation as Location
        }
    }

    private fun dummyLocation() :Location{

        val l = Location("")
        l.latitude = 0.0
        l.longitude = 0.0
        return l
    }
}